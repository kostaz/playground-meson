Playing around with meson and ninja build system.

Gotchas...
==========
- GCC build (default): `meson build-gcc; ... ninja -v`
- Clang build: `CC=clang CXX=clang++ meson build-clang; ... ninja -v`
